Frontend
========

A starter kit for responsive web projects

Requirements
------------

* A Mac or Windows PC or a dog
* Installed [Mixture App](http://mixture.io/#download)
* Installed Ruby (if you're on Windows) [Rails Installer](http://railsinstaller.org)
* Installed Ruby Gems: [Susy](http://susy.oddbird.net/guides/getting-started) and [Compass Retinator](https://github.com/restorando/compass-retinator)

Configuration
-------------

There are a myriad of frontend tools currently, but at the time of writing, Mixture app is the only cross platform solution. There is a 14 day trial available after that, you'll need a license at $40 USD.

Mixture
-------
Mixture app uses its own install of Ruby, Compass and Sass.

Normally this would be fine but our setup uses other Ruby Gems (Compass Retinator) that Mixture doesn't yet support by default.

In this scenario, we point Mixture to our custom Ruby. Details on how to set this can be found at [http://docs.mixture.io/custom-ruby](http://docs.mixture.io/custom-ruby)

Using Mixture gives other advantages. Its built-in web server allows other devices (phones & tablets) to connect and check how the site responds on that device. More so, Mixture will actually force update those devices as you develop work.

CSS standards
---------

* CSS should use BEM syntax
* Omit the protocol from embedded resources
* Use only lowercase for selectors, attributes and properties
* Remove trailing whitespace
* Use meaningful class names
* Mark todos and action items with TODO
* Omit unit specification after "0" values
* Use 3 character hexadecimal notation where possible
* Each CSS attribute should have its own line
* Use CSS shorthand
* Comment everything! This is the best documentation for any project. It will allow you (and more importantly) future developers to quickly see the intention with given rules
* Alphabetize selector CSS attributes
* Avoid using browser hacks - if you need to as a short-term fix, add them to _shame.scss until you have time to refactor appropriately and then remove them when refactored
* Avoid using !important on anything. There are times when !important is acceptable (state/error styles for example)
* Don't use pixels, ems are a better unit of measurement. There's an em calculator function within our Sass project to convert pixels - use it!
* Avoid using id selectors in CSS (by all means use them in JS and HTML identifiers). id selectors increase specificity and can end up causing your more headaches in the future
* Use a semicolon after each declaration
* Use a space after a property name's colon
* Use single quotation marks for attribute selectors and property values.

SMACSS(ish)
------

The idea behind a SMACSS approach to frontend development is that it keeps CSS more organised and structured, particularly on large scale projects.

We say SMACSS(ish) because we don't follow it to the letter. We have no layout styles (preferring that things are modules) and we have no 'themes' because our websites aren't theme-able (this is really more apt for applications).

**Naming conventions**

Files that are imported and not compiled to standalone files should begin with an underscore. In the case of a CSS module for a component, the name of the component

**Module structure**

If we take the `_example-module.scss` as an example:

	.example {

	}
	// this is the module definition

	// if you have --modifier versions of the module (below)
	// consider extracting common parts to a
	// a %placeholder class and use @extend

	.example--modifier {
		@extend %example;
	}

    // extends the root module to create a different standalone module
    // for example .example--large
    // only @extend within modules - @extend anywhere else defeats the
    // purpose of extrapolating a module into its own file

    .example__subcomponent {

    }

    // a subcomponent of .example module

    .example--is-hidden {

    }

    // a state specific to this module and can't use generic states found in _state.scss

    // this call is applied on top of the module class (by JS) so doesn't need
    // to extend the original module

    // resulting HTML could look like this
    // <article class="example example--is-hidden">

Susy
----
Susy is a Ruby gem that makes responsive development easier. It allows us to create custom, semantic, responsive grids. Built-in we have the `at-breakpoint` mixin and an array of [grid helpers](http://susy.oddbird.net/guides/reference/#ref-helper).

Media Queries
------
There are currently 5 default breakpoints defined within the project - these can be added to depending on the project but these are pretty common.

- mobile first (which isn't a breakpoint really as there is no media query - this is the default
- narrow (tablets)
- narrow-only
- narrow-max
- wide (desktop)

These breakpoints are defined within our `_config.scss` and are defined in ems by dividing the given pixel width by the `$base-font-size` (typically 16) and then setting the number of Susy columns.

The targeting of breakpoints happens right inside the modules. The first styles encountered within a module are for mobile first, if we need to alter these at a breakpoint we do this

	.module {
		width: 90%;
		@include at-breakpoint($narrow) {
			width: 25%;
			float: left;
			text-align: right;
		}
	}

In this example, the `at-breakpoint` mixin is from the Susy gem and the `$narrow` is a breakpoint set in `_config.scss`

Internet Exploder
-----

IE6 is dead and hopefully IE7 and IE8 will be soon enough as well. However, using Sass allows us to deliver easy options patching bugs in those browsers by serving specific stylesheets to each.

IE-specific styles live right inside the module they belong to:

	.module {
		color: $black;

		@if $ie7 {
			color: $white;
		}
	}

In the example above, Sass will generate ie7.css and the text colour of that module will be white, rather than black.

Images
-----

Where possible, make use of background images. The Compass Retinator makes this real easy. Simply add your background image into `src/images/icon` and the retina version into `src/images/icon2x` then, where you want to use that image in your Sass, you do this:


	.my-class {
		@include sprite(logo, $hover_state: true);
	}

In this instance, the first parameter `logo` is the name of our image (minus the file extension), we're setting the hover state parameter to true, therefore we need an image called logo_hover.png in the same dir also.

The Compass Retinator does its magic, generates an image sprite and calculates the background position accordingly.

Further reading
------------------------

* [Susy](http://susy.oddbird.net/)
* [Scalable modular CSS](http://smacss.com)
* [Getting your head around BEM syntax](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
* [A new front-end methodology: BEM](http://coding.smashingmagazine.com/2012/04/16/a-new-front-end-methodology-bem/)
* [Maintainable CSS with BEM](http://integralist.co.uk/Maintainable-CSS-with-BEM.html)
* [Responsive tips from the BBC](http://www.creativebloq.com/web-design/responsive-web-design-tips-bbc-news-9134667)

Credits
------------------------
- Typography styles from [Sassline](http://sassline.com)

TODO
------------------------
- look at integrating GulpJS
- look at integrating Wraith